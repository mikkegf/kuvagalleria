<?php require_once 'inc/top.php'; ?>
   
<?php
    $folder = 'uploads';
    $handle = opendir($folder);
    if ($handle) {
        print "<div id='images'></div>";
        print "<div class='card-group'>";
        print "<div class='row'>";
        while (false !== ($file = readdir($handle))) {
            $type = pathinfo($file, PATHINFO_EXTENSION);
            if (strtoupper($type) === 'PNG' || strtoupper($type) === 'JPG' || strtoupper($type) === 'JPEG') {
                $path = $folder . '/' . $file;
                $thumbs_path = $folder . '/thumbs/' . $file;
                ?>
                <div class="card p-2 col-sm-6 col-md-4 col-lg-3">
                    <a data-fancybox="gallery" href="<?php print $path; ?>">
                        <img class="card-img-top" src="<?php print $path; ?>">

                    </a>
                    <div class="card-body">
                        <p class="card-text"><?php print $file; ?></p>
                    </div>

                </div>
                <?php
               
            }
        }
        closedir($handle);
        
       
    }


     
?>
<?php require_once 'inc/bottom.php'; ?>
